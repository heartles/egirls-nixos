{ config, pkgs, ... }:

#necessary prep work:
# GRANT CONNECT ON DATABASE misskey TO "misskey-backup";
# GRANT SELECT ON ALL TABLES IN SCHEMA public TO "misskey-backup";
# GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO "misskey-backup";
#
# TODO: automate this cause it needs to be done whenever db schema changes
let
  user = "misskey-backup";
  group = user;

  backupConfigFile = "/etc/misskey-backup/conf";
  s3Cfg = "/etc/misskey-backup/s3cfg";

  tarRedisStdoutCmd = pkgs.writeShellScript "backup-misskey-redis" ''
    tar -cz -C /var/lib/redis-misskey .
  '';

  notifyEmailPkg = pkgs.writeShellApplication {
    name = "notify-email";

    runtimeInputs = [ pkgs.system-sendmail pkgs.coreutils ];

    text = ''
      from="noreply+$1@admin.egirls.gay"
      subject="$2"
      text="$3"

      to="admin@heartles.xyz"

      header="$(cat <<EOEMAIL
      To: $to
      From: $from
      Subject: $subject
      EOEMAIL
      )"

      email="$(cat <<EOEMAIL
      $header

      $text

      EOEMAIL
      )"

      echo "Sending message:"
      cat <<< "$header"

      sendmail -f "$from" "$to" <<< "$email"
    '';
  };
in {
  users.users."${user}" = {
    isSystemUser = true;
    inherit group;
    extraGroups = [ "misskey" "redis-misskey" "systemd-journal" ];
  };
  users.groups."${group}" = { };
  services.postgresql.ensureUsers = [{ name = user; }];

  systemd.services.misskey-backup = {
    description = "Misskey backup";

    restartIfChanged = false;
    unitConfig.X-StopOnRemoval = false;

    serviceConfig.User = user;
    serviceConfig.Type = "oneshot";

    startAt = "weekly";

    path = with pkgs; [
      gzip
      config.services.postgresql.package
      s3cmd
      coreutils
      gnutar
      age
    ];

    script = ''
      set -o pipefail

      ageRecipient="age17ckyc69njpryytc63ynn545jswyucg28k5xg3043g3j6q38dxqwq0wzhm2"
      bucket="$(grep 'bucket=' < "${backupConfigFile}" | sed 's/bucket \?= \?//g')"
      prefix="$(grep 'prefix=' < "${backupConfigFile}" | sed 's/prefix \?= \?//g')"

      s3Dir="s3://$bucket/$prefix""misskey-$(date +'%Y-%m-%dT%H.%M.%S')"
      echo "Uploading backups to '$s3Dir'"

      function upload () {
        name="$1"

        age -r "$ageRecipient" | s3cmd put --config "${s3Cfg}" - "$s3Dir/$name.age" --multipart-chunk-size-mb=100
      }

      echo "Uploading config"
      tar -cz -C /srv/misskey/.config . | upload "config.tar.gz"

      echo "Uploading redis database..."
      /run/wrappers/bin/sudo ${tarRedisStdoutCmd} | upload "redis.tar.gz"

      echo "Dumping postgres database..."
      pg_dump misskey | gzip | upload "pg_dump.sql.gz"

      echo "Backup complete to '$s3Dir'"
    '';

    serviceConfig.ExecStopPost = let
      script = pkgs.writeShellScript "backup-notify" ''
        invocationId="$(systemctl show --value -p InvocationID misskey-backup.service)"
        logs="$(journalctl _SYSTEMD_INVOCATION_ID="$invocationId" -u misskey-backup.service)"

        if [ "$SERVICE_RESULT" = "success" ]; then
          ${notifyEmailPkg}/bin/notify-email "backup" "SUCCESS: Misskey Backup Notification" "$(cat <<EOMSG
        A backup process has succeeded. Logs to follow:

        $logs

        EOMSG
        )"
        else
          ${notifyEmailPkg}/bin/notify-email "backup" "FAILURE: Misskey Backup Notification" "$(cat <<EOMSG
        A backup process has failed. Logs to follow:

        $logs

        EOMSG
        )"
        fi
      '';
    in "${script}";

    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    requires = [ "postgresql.service" ];
  };

  systemd.timers.misskey-backup = { timerConfig.Persistent = true; };

  security.sudo.extraRules = [{
    groups = [ group ];
    commands = [{
      command = "${tarRedisStdoutCmd}";
      options = [ "NOPASSWD" ];
    }];
  }];
}
