{
  users.groups.ogdo = { members = [ "nginx" "jaina" ]; };
  services.nginx = {
    enable = true;

    # ꙮ.run
    virtualHosts = let
      listen = [
        {
          addr = "0.0.0.0";
          port = 80;
        }
        {
          port = 80;
          addr = "[::]";
        }
        # deliberately avoid listening with https
        {
          addr = "0.0.0.0";
          port = 443;
          ssl = true;
        }
        {
          port = 443;
          ssl = true;
          addr = "[::]";
        }
      ];
      rejectSSL = true;
    in {
      "xn--xx8a.run" = {
        inherit listen rejectSSL;
        root = "/srv/ogdo";

        extraConfig = ''
          error_page 404 /;
          access_log /var/log/nginx/ogdo.log combined;
          add_header 'Cache-Control' 'no-cache';
        '';

        locations."/" = { index = "/index.html"; };
        locations."~ ^/.+" = {
          root = "/srv/ogdo/served-files";
          tryFiles = "$uri =404";
          extraConfig = ''
            default_type application/pdf;
          '';
        };
      };

      "ogdo.run" = {
        inherit listen rejectSSL;
        locations."/".return = "301 http://xn--xx8a.run$request_uri";
      };

      "ꙮ.run" = {
        inherit listen rejectSSL;
        locations."/".return = "301 http://ogdo.run$request_uri";
      };
    };
  };
}
