{ pkgs, ... }@inputs: {
  imports = [
    ./hardware-configuration.nix
    ./misskey-service.nix
    ./heartles-xyz-proxy.nix
    ./ogdo.nix
    ./postfix.nix
    ./nebula.nix
    ./backup.nix
  ];

  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  system.autoUpgrade = {
    enable = true;
    flake = inputs.self.outPath;
    flags = [
      "--update-input"
      "nixpkgs"
      "--no-write-lock-file"
      "-L" # print build logs
    ];
    dates = "02:00";
    randomizedDelaySec = "45min";
  };

  environment.systemPackages = with pkgs; [
    kitty.terminfo
    kakoune
    vim
    git
    htop
    wget
    less
    killall
    screen
  ];

  users.users.jaina = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };

  systemd.network.enable = true;
  networking.useNetworkd = true;
  systemd.network.networks."10-wan" = {
    matchConfig.Name = "enp1s0";
    networkConfig.DHCP = "ipv4";
    address = [ "2a01:4ff:1f0:e4bd::/64" ];
    routes = [{ routeConfig.Gateway = "fe80::1"; }];
  };
  networking.interfaces."enp1s0".useDHCP = true;

  boot.tmp.cleanOnBoot = true;
  boot.loader.grub.device = "/dev/sda";
  zramSwap.enable = true;
  networking.hostName = "egirls";
  networking.domain = "gay";
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG6udktq60NydvPg6nnZpwxvQ6scC1HkqTZIG+ORt/HN"
  ];
  services.openssh.settings.PasswordAuthentication = false;
  services.openssh.ports = [ 9922 ];
  networking.firewall.allowedTCPPorts = [ 9922 ];
  services.atd.enable = true;
  system.stateVersion = "23.11";
}
