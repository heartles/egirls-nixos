{ pkgs, lib, ... }:

{
  environment.systemPackages = [ pkgs.nebula ];
  services.nebula.networks.home = {
    enable = true;
    isLighthouse = true;
    cert = "/var/lib/nebula/node.crt";
    key = "/var/lib/nebula/node.key";
    ca = "/var/lib/nebula/ca.crt";

    firewall.inbound = [{
      host = "any";
      port = "any";
      proto = "any";
    }];
    firewall.outbound = [{
      host = "any";
      port = "any";
      proto = "any";
    }];
  };

  networking.firewall.allowedTCPPorts = [ 4242 ];
  networking.firewall.allowedUDPPorts = [ 4242 ];
}
