{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, unstable, ... }@attrs: {
    nixosConfigurations."egirls" = nixpkgs.lib.nixosSystem rec {
      system = "x86_64-linux";
      pkgs = let
        cfg = {
          inherit system;
          config.allowUnfree = true;
          overlays = [ (final: prev: { unstable = import unstable cfg; }) ];
        };
      in import nixpkgs cfg;

      specialArgs = attrs;
      modules = [
        ./configuration.nix

        # set nix shell nixpkgs to flake nixpkgs
        ({ config, pkgs, options, ... }: {
          nix.registry.nixpkgs.flake = nixpkgs;
        })
      ];
    };
  };
}
