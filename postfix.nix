{ pkgs, config, ... }:

{
  services.postfix = {
    enable = true;
    enableSubmission = true;
    domain = config.networking.fqdn;

    config = {
      inet_interfaces = "loopback-only";
      smtpd_milters = "inet:127.0.0.1:8891";
      non_smtpd_milters = "$smtpd_milters";
      milter_default_action = "accept";
    };
  };

  services.opendkim = {
    enable = true;
    selector = "default";
    socket = "inet:8891@127.0.0.1";
    domains = "csl:${config.networking.fqdn},admin.${config.networking.fqdn}";
  };
}
