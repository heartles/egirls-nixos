{ config, lib, ... }: {
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  security.acme = {
    acceptTerms = true;
    defaults.email = "admin+acme@heartles.xyz";
    certs."STAR.home.heartles.xyz" = {
      domain = "*.home.heartles.xyz";
      dnsProvider = "namecheap";
      credentialsFile = "/etc/nixos-secrets/namecheap-acme";
      group = "nginx";
    };
  };

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    upstreams."home-server".extraConfig = ''
      include /etc/nixos-secrets/nginx-home-server-upstream.conf;
    '';
    virtualHosts = {
      "*.home.heartles.xyz" = {
        listen = [
          {
            port = 443;
            addr = "0.0.0.0";
            ssl = true;
          }
          {
            port = 80;
            addr = "0.0.0.0";
          }
          {
            port = 443;
            addr = "[::]";
            ssl = true;
          }
          {
            port = 80;
            addr = "[::]";
          }
        ];

        useACMEHost = "STAR.home.heartles.xyz";
        forceSSL = true;

        locations."/" = {
          proxyWebsockets = true;
          proxyPass = "https://home-server";
          extraConfig = ''
            proxy_ssl_name $host;
          '';
        };
      };
    };
  };
}
